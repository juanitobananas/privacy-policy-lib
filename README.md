# Privacy Policy Android Library

Here is my first attempt to create an Android Library :)

This library helps you create a very simple Privacy Policy Activity. Mostly I have developed this for myself,
because I don't want to implement the same Activity for all my apps. Not that I have a lot, but it's is still a PITA.

You can use the sections I defined or use create an HTML string and add it as 'Custom Section'. You can also use both.

# Adding to your Android Studio Project

## As gradle dependency using jitpack.io

Follow these instructions: https://jitpack.io/#com.gitlab.juanitobananas/privacy-policy-lib/


## As a git submodule

Add the library as a git submodule:

```
cd ~/git/my-cool-project
git submodule add https://gitlab.com/juanitobananas/privacy-policy-lib.git
```

Note: You can (should) git commit that change to your original git repo now.

Add the git submodule as a module in Android Studio:

- File -> New -> Import Module...

- Select the `~/git/my-cool-project/privacy-policy-lib` as the *source directory*

A `privacypolicy` module should appear in the Android view under the app module.

Now you just need to add it as a gradle dependency:

- In app/build.gradle add `implementation project(path: ':privacypolicy')` to the dependencies section.

And that's it!

### Some basic stuff about git submodules

- To clone your project with git, you will have to use the `--recurse-submodules` option.

- You can do a git pull/checkout/whatever *inside* the privacy-policy-lib directory to update it or change to a different commit.

- If you have several submodules you'll probably already know this but you can update them all to the latest upstream commit with `git submodule update --recursive --remote`

- If your app is published to F-Droid, you will have to add a `submodules=yes` to your metadata so that F-Droid knows how to build your app.

# Usage

```
new PrivacyPolicyBuilder()
        .withIntro(getString(R.string.app_name), "Juanito")
        .withUrl("https://example.org/juanito/app/privacy-policy")
        .withFDroidSection()
        .withGooglePlaySection()
        .withEmailSection("juanito@example.org")
        .withCustomSection("<h1>Yeah, just use html</h1><p>Hint: If you want to add this from an xml string resource, you'll have to surround it with a CDATA tag.</p>")
        .start(getApplicationContext());

```
