package com.jarsilio.android.privacypolicy;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

public abstract class SimpleHtmlAbstractActivity <T extends SimpleHtmlAbstractActivityBuilder> extends AppCompatActivity {
    private static final String TAG = "SimpleHtmlActivity";

    private T builder;

    abstract String getActivityTitle();
    abstract String getHtmlContent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setBuilder();
        setTitle(getActivityTitle());
        setContentView(R.layout.activity_simple_html_abstract);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Add back button
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        TextView textView = new TextView(this);
        textView.setText(fromHtml(getHtmlContent()));
        textView.setMovementMethod(LinkMovementMethod.getInstance());

        LinearLayout linearLayout = findViewById(R.id.simple_html_abstract_linear_layout);
        linearLayout.addView(textView);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            super.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setBuilder() {
        Intent intent = getIntent();
        builder = intent.getParcelableExtra("builder");
        if (builder == null) {
            Log.e(TAG, "Parcelable 'builder' cannot be null. You can only start PrivacyPolicyActivity using the Builder");
            throw new IllegalArgumentException("Parcelable 'parcel' cannot be null while starting PrivacyPolicyActivity. You can only start PrivacyPolicyActivity using the Builder");
        }
    }

    @NonNull
    private static Spanned fromHtml(@NonNull String source) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY);
        } else {
            //noinspection deprecation
            return Html.fromHtml(source);
        }
    }

    T getBuilder() {
        return builder;
    }
}
